from images import size, create_blank


def brillo(image: list[list[tuple[int, int, int]]], change: float) -> list[list[tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            original_r = pixel[0]
            original_g = pixel[1]
            original_b = pixel[2]
            brillo_r = int(original_r * change)
            brillo_g = int(original_g * change)
            brillo_b = int(original_b * change)
            pixel_brillo = (brillo_r, brillo_g, brillo_b)
            pixel_brillo_lista = list(pixel_brillo)
            for i in range(len(pixel_brillo)):
                if pixel_brillo_lista[i] < 0:
                    pixel_brillo_lista[i] = 0
                elif pixel_brillo_lista[i] > 255:
                    pixel_brillo_lista[i] = 255
            new_image[x][y] = pixel_brillo
    return new_image
