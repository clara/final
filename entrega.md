# ENTREGA CONVOCATORIA ENERO
# CALRA GARCÍA PEÑA c.garciape.2023@alunmos.urjc.es
VIDEO: https://youtu.be/FCD6ZlspT3c

REQUISITOS MINIMOS: 
- Método change_colors
- Método rotate_right
- Método mirror
- Método rotate_colors
- Método blur
- Método shift
- Método crop
- Método grayscale
- Método filter
- Programa transforms_simple
- Programa transforms_args
- Programa transforms_multi

REQUISITOS OPCIONALES:
- Método cambiar a colores sepia
- Método cambiar la luminosidad
- Método cambiar el contraste
- Programa interactivo

COMENTARIOS:
La duracion  mínima del vídeo, para mí, era muy escasa. 
He intentado hacerlo lo mejor posible dentro del tiempo. 
Me disculpo si no esta bien explicado. 
CUANDO HICE EL FORK NO ESTABAN TODAVIA LOS TESTS DE TRANSFORM_ARGS Y TRANSFORMS_MULTI, LOS HE INTENTADO METER EN LA CARPETA PERO NO HE SIDO CAPAZ POR LO QUE FALTAN EN EL TRABAJO Y NO HE PODIDO COMPROBAR MI CODIGO CON ELLO 