from transforms import *
from images import read_img, write_img
from opcional1 import brillo
from opcional2 import sepia
from opcional3 import contraste


pixels = read_img("cafe.jpg")
new_pixels = contraste(pixels, 5)
write_img(new_pixels, "cafe_contraste3.jpg")


