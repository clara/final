import sys
from transforms import *
from images import read_img, write_img
from opcional2 import sepia
from opcional1 import brillo
from time import sleep

RESET = "\033[0m"
ROJO = "\033[91m"
VERDE = "\033[92m"
AMARILLO = "\033[93m"
AZUL = "\033[94m"
MAGENTA = "\033[95m"
CIAN = "\033[96m"
BLANCO = "\033[97m"


def print_color(mensaje, color):
    print(color + mensaje + RESET)


def print_menu_principal():
    print("\t 0. Salir del programa")
    print("\t 1. Leer imagen a transformar")
    print("\t 2. Guardar imagen en archivo")
    print("\t 3. Aplicar transformacion")


def print_menu_leer_imagen():
    print("\t 0. Salir del programa")
    print("\t 1. Leer imagen a transformar")


def print_menu_transformaciones():
    print_color("TRANSFORMACIONES: ", AMARILLO)
    print()
    print("1. Mirror")
    print("2. Blur")
    print("3. Rotate right")
    print("4. Grayscsale")
    print("5. Change colors")
    print("6. Shift")
    print("7. Rotate colors")
    print("8. Filter")
    print("9. Crop")
    print("10. Change brightness")
    print("11. Transform to sepia")


def guardar_imagen(pixels: list[list[tuple[int, int, int]]]):
    filename = input("Introduce el nombre del archivo al que quieres guardar la imagen: ")
    write_img(pixels, filename)


def leer_imagen():
    try:
        filename = input("Introduce el nombre de la imagen a transformar: ")
        pixels = read_img(filename)
        return pixels
    except FileNotFoundError:
        print_color("La imagen no existe! ", ROJO)
        sleep(2)
        leer_imagen()


def do_mirror(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_pixels = mirror(pixels)
    return new_pixels


def do_blur(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_pixels = blur(pixels)
    return new_pixels


def do_rotate_right(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_pixels = rotate_right(pixels)
    return new_pixels


def do_grayscale(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_pixels = grayscale(pixels)
    return new_pixels


def do_change_colors(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print()
    print_color("Introduce los valores del color que quieres cambiar: ", AMARILLO)
    r1 = int(input("introduce el valor R: "))
    g1 = int(input("introduce el valor G: "))
    b1 = int(input("introduce el valor B: "))

    print_color("Introduce los valores del color al que quieres cambiar: ", AMARILLO)
    r2 = int(input("introduce el valor R: "))
    g2 = int(input("introduce el valor G: "))
    b2 = int(input("introduce el valor B: "))

    new_pixels = change_colors(pixels, (r1, g1, b1), (r2, g2, b2))
    return new_pixels


def do_shift(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print()
    desp_hor = int(input("introduce el desplazamiento horizontal: "))
    desp_ver = int(input("introduce el desplazamiento vertical: "))

    new_pixels = shift(pixels, desp_hor, desp_ver)
    return new_pixels


def do_rotate_colors(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print()
    increment = int(input("introduce el desplazamiento vertical: "))

    new_pixels = rotate_colors(pixels, increment)
    return new_pixels


def do_filter(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print()
    r = float(input("Introduce el multiplicador para R: "))
    g = float(input("Introduce el multiplicador para G: "))
    b = float(input("Introduce el multiplicador para B: "))

    new_pixels = filter(pixels, r, g, b)
    return new_pixels


def do_crop(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print()
    print_color("Introduce los valores del rectángulo a recortar: ", AMARILLO)
    x = int(input("Introduce la COLUMNA de la esquina superior izquierda: "))
    y = int(input("Introduce la FILA de la esquina superior izquierda: "))
    width = int(input("Introduce la ANCHURA: "))
    height = int(input("Introduce la ALTURA: "))

    new_pixels = crop(pixels, x, y, width, height)
    return new_pixels


def do_brillo(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print()
    change = float(input("introduce el  multiplicador (>1 para aumentar el brillo, <1 para disminuirlo): "))

    new_pixels = brillo(pixels, change)
    return new_pixels


def do_sepia(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_pixels = sepia(pixels)
    return new_pixels


def aplicar_transformacion(pixels: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    print_menu_transformaciones()

    try:
        opcion = int(input("Introduce una transformacion: "))

        if opcion == 1:
            new_pixels = do_mirror(pixels)
        elif opcion == 2:
            new_pixels = do_blur(pixels)
        elif opcion == 3:
            new_pixels = do_rotate_right(pixels)
        elif opcion == 4:
            new_pixels = do_grayscale(pixels)
        elif opcion == 5:
            new_pixels = do_change_colors(pixels)
        elif opcion == 6:
            new_pixels = do_shift(pixels)
        elif opcion == 7:
            new_pixels = do_rotate_colors(pixels)
        elif opcion == 8:
            new_pixels = do_filter(pixels)
        elif opcion == 9:
            new_pixels = do_crop(pixels)
        elif opcion == 10:
            new_pixels = do_brillo(pixels)
        elif opcion == 11:
            new_pixels = do_sepia(pixels)

        return new_pixels

    except ValueError:
        print_color("No has introducido bien el número! ", ROJO)
        aplicar_transformacion(pixels)
    except UnboundLocalError:
        print_color("Opción inválida! ", ROJO)
        sleep(2)
        aplicar_transformacion(pixels)


def main():

    SALIR = 0
    opcion = -1
    pixels = []
    new_pixels = []

    while opcion != SALIR:
        #si la imagen se ha leido
        if pixels != []:
            print_menu_principal()
            try:
                opcion = int(input("Introduce una opción: "))
                if opcion == 1:
                    pixels = leer_imagen()
                    new_pixels = pixels
                elif opcion == 2:
                    guardar_imagen(new_pixels)
                elif opcion == 3:
                    new_pixels = aplicar_transformacion(new_pixels)
            except ValueError:
                print_color("Introduce un entero! ", ROJO)

        else:
            print_menu_leer_imagen()
            try:
                opcion = int(input("Introduce una opción: "))
                if opcion == 1:
                    pixels = leer_imagen()
                    new_pixels = pixels
            except ValueError:
                print_color("Introduce un entero! ", ROJO)


if __name__ == '__main__':
    main()