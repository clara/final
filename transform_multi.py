import sys
from transforms import *
from images import write_img, read_img
'''Programa transform_multi.py, que funcionará como el anterior, pero admitiendo varios nombres de función 
transformadora, si es caso seguidos por sus parámetros. El primer argumento será la imagen a transformar, 
el siguiente la primera función de transformación, seguida si es caso por sus argumentos, seguidos de la segunda 
función de transformación, y así sucesivamente. El programa deberá leer la imagen, aplicarle las transformaciones 
indicadas, y producir la imagen resultante, que tendrá el mismo nombre que se indica en los programas anteriores. '''

transform_n_arguments = {"mirror": 0,
                         "blur": 0,
                         "rotate_right": 0,
                         "greyscale": 0,
                         "change_colors": 6,
                         "shift": 2,
                         "rotate_colors": 1,
                         "filter": 3,
                         "crop": 4}


def get_filename(filename):
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename


def is_letter(c: chr) -> bool: # toda esta en función de la tabla assi
    return 'a' <= c and c <= 'z' or 'A' <= c and c <= 'Z'


def split_transforms(transforms):
    # ["shift", "1", "2", "rotate_right", "rotate_colors", "5"]
    # [ ["shift", "1", "2"], ["rotate_right"], ["rotate_colors", "5"] ]
    splitted = []
    transform = []

    for s in transforms:
        primer_char = s[0]
        if is_letter(primer_char):
            # acaba una transformacion
            splitted.append(transform)
            # empieza una transformacion
            transform = []

        transform.append(s)
    splitted.append(transform)
    return splitted


def apply_transform(pixels, transforms):
    transform = transforms[0]

    if transform == "filter":
        args = [float(i) for i in transforms[1:]]
    else:
        args = [int(i) for i in transforms[1:]]

    if transform_n_arguments[transform] == len(args):
        if transform == "rotate_right":
            new_pixels = rotate_right(pixels)
        elif transform == "mirror":
            new_pixels = mirror(pixels)
        elif transform == "blur":
            new_pixels = blur(pixels)
        elif transform == "grayscale":
            new_pixels = grayscale(pixels)
        elif transform == "change_colors":
            new_pixels = change_colors(pixels, (args[0], args[1], args[2]), (args[3], args[4], args[5]))
        elif transform == "shift":
            new_pixels = shift(pixels, args[0], args[1])
        elif transform == "rotate_colors":
            new_pixels = rotate_colors(pixels, args[0])
        elif transform == "filter":
            new_pixels = filter(pixels, args[0], args[1], args[2])
        elif transform == "crop":
            new_pixels = crop(pixels, args[0], args[1], args[2], args[3])

        return new_pixels

    else:
        print("[!] Número incorrecto de argumentos")
        sys.exit(1)


def main():
    if len(sys.argv) < 3:
        print(f"\n[!] uso: {sys.argv[0]} <imagen> <transform(s)>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        pixels = read_img(filename)
        new_pixels = pixels
        transforms_split = split_transforms(transforms)

        for transform in transforms_split:
            new_pixels = apply_transform(new_pixels, transform)

        new_filename = get_filename(filename)
        write_img(new_pixels, new_filename)

        print("[+] Transformacion exitosa!")


if __name__ == '__main__':
    main()