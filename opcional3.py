from images import create_blank, size


def contraste(image: list[list[tuple[int, int, int]]], contraste: int) -> list[list[tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)
    c = (100.0 + contraste) / 100.0
    c = c**2  # c al cuadrado

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            original_r = pixel[0]
            original_g = pixel[1]
            original_b = pixel[2]
            contraste_r = round(((original_r / 255 - 0.5) * c) + 0.5) * 255
            contraste_g = round(((original_g / 255 - 0.5) * c) + 0.5) * 255
            contraste_b = round(((original_b / 255 - 0.5) * c) + 0.5) * 255
            pixel_contraste = (contraste_r, contraste_g, contraste_b)
            pixel_contraste_lista = list(pixel_contraste)
            for i in range(len(pixel_contraste)):
                if pixel_contraste_lista[i] < 0:
                    pixel_contraste_lista[i] = 0
                elif pixel_contraste_lista[i] > 255:
                    pixel_contraste_lista[i] = 255
            new_image[x][y] = pixel_contraste
    return new_image


