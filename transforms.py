import images


def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple) -> list[list[tuple[int, int, int]]]:
    ''' Método change_colors, que producirá una nueva imagen con los colores cambiados.
    Aceptará tres parámetros, que serán: la imagen original, el color a cambiar,
    y color por el que será cambiado. devolverá una imagen.
    Las imágenes serán matrices (listas de listas) de tuplas (R, G, B), y los colores serán tuplas (R, G, B) '''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            if image[x][y] == to_change:
                new_image[x][y] = to_change_to
            else:
                new_image[x][y] = image[x][y]

    return new_image


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    '''Método rotate_right, que producirá una nueva imagen girada 90 grados hacia la derecha.
    Aceptará un parámetro, que será la imagen a girar, y devolverá la imagen girada'''
    original_width, original_height = images.size(image)
    new_width, new_height = original_height, original_width
    new_image = images.create_blank(new_width, new_height)

    for x in range(new_width):
        for y in range(new_height):
            new_image[x][y] = image[y][original_height - 1 - x]
    return new_image


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    '''Método mirror, que producirá una nueva imagen "espejada" según un eje vertical situado en la mitad de la imagen.
    Aceptará un parámetro, que será la imagen a espejar, y devolverá la imagen espejada'''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            new_image[x][y] = image[width - 1 - x][y]
    return new_image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    '''Método rotate_colors, que producirá una nueva imagen con los colores cambiados. Para cambiarlos, sumará a cada uno de los componentes RGB
    el número que se le indique como incremento, teniendo en cuenta que si el valor llega a 255, tendrá que volver a empezar en 0
    (esto es, se incrementará módulo 256). El incremento podrá ser también un valor negativo. Aceptará dos parámetros, que serán: la imagen original,
    y incremento a realizar a los valores RGB de cada pixel. Devolverá una imagen'''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            original_pixel = image[x][y]
            new_pixel = ((original_pixel[0] + increment) % 256, (original_pixel[1] + increment) % 256, (original_pixel[2] + increment) % 256)
            new_image[x][y] = new_pixel

    return new_image


def find_neighbours(x: int, y: int, width: int, height: int) -> list[tuple[int, int]]:
    neighbours = []
    final_neighbours = []
    neighbours.append( (x, y) )
    neighbours.append( (x - 1, y - 1) )
    neighbours.append( (x - 1, y) )
    neighbours.append( (x - 1, y + 1) )
    neighbours.append( (x, y - 1) )
    neighbours.append( (x , y + 1) )
    neighbours.append( (x + 1, y - 1) )
    neighbours.append( (x + 1, y) )
    neighbours.append( (x + 1, y + 1) )

    for n in neighbours:
        if not ((n[0] < 0) or (n[1] < 0) or (n[0] >= width) or (n[1] >= height)):
            final_neighbours.append(n)

    return final_neighbours


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    '''Método blur que creará una nueva imagen donde el valor RGB de cada pixel será el valor medio de los valores RGB
    de los pixels que tiene encima, debajo, a la derecha y a la izquierda (salvo que esté en los bordes,
    donde obviamente no se considerá el valor correspondiente: pòr ejemplo, si está totalmente arriba,
    no se podrá usar el valor del pixel de encima, porque no lo hay). Aceptará un parámetro, que será la imagen a
    modificar, y devolverá la imagen modificada'''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            neighbours = find_neighbours(x, y, width, height)
            med_red = 0
            med_green = 0
            med_blue = 0

            for n in neighbours:
                pixel = image[n[0]][n[1]]
                med_red += pixel[0]
                med_green += pixel[1]
                med_blue += pixel[2]

            med_red = round(med_red / len(neighbours))
            med_green = round(med_green / len(neighbours))
            med_blue = round(med_blue / len(neighbours))

            new_image[x][y] = (med_red, med_green, med_blue)

    return new_image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]: #hay que sumar el numero que se quiera al numero que ocupa la coumna o la fila
    '''Método shift, que desplazará la imagen el número de pixels que se indique, en el eje horizontal o vertical, según el parámetro que se indique.
    Aceptará tres parámetros, que serán: la imagen a modificar, la cantidad de pixels a desplazr horizontalmente (hacia la derecha si es positivo, hacia la izquierda si es negativo),
    y la cantidad de pixels a desplazar verticalmente (hacia arriba si es positivo, hacia abajo si es negativo). Devolverá una imagen'''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            new_image[x][y] = image[(x - horizontal) % width][(y + vertical) % height]
    return new_image


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    '''Método crop, que creará una nueva imagen que contendrá solo los pixels que se encuentren dentro de un rectángulo que se especifique.
    Aceptará cinco parámetros, que serán: la imagen a recortar, y cuatro parámetros para escpecificar el rectángulo a recortar.
    El rectángulo se especificará dando las coordenadas x, y de su esquina superior izquierda, su ancho, y su altura. Devolverá una imagen'''
    new_image = images.create_blank(width, height)

    for c in range(width):
        for r in range(height):
            new_image[c][r] = image[x + c][y + r]

    return new_image


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    '''Método 'greyscale', que creará una nueva imagen que contendrá la imagen original,
    pero en escala de grises. Aceptará un solo parámetros, que será la imagen a convertir.
    Devolverá una imagen. Para convertir un píxel RGB a gris, basta con calcular la media de los tres valores
    de la tupla RGB, y asignar ese valor a los tres valores de la tupla RGB del pixel equivalente en la imagen resultante.'''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            mean = (pixel[0] + pixel[1] + pixel[2]) // 3
            new_image[x][y] = (mean, mean, mean)

    return new_image


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    '''Método filter, que creará una nueva imagen que contendrá la imagen original, pero con un filtro aplicado a todos sus pixels.
    El filtro se especificará como un multiplicador para cada valor RGB. Por lo tanto, la función aceptará cuatro parámetros,
    que serán la imagen a convertir, y el multiplicador de filtro para cada uno de los componentes RGB. Cada pixel de la imagen
    resultante tendrá una tupla RGB igual a la de la imagen original, con cada componente RGB multiplicado por su multiplicador correspondiente,
    teniendo en cuenta que si el resultado es mayor que el valor máximo posible para el componente, el valor será ese máximo posible
     (normalmente, 255). Devolverá una imagen.'''
    width, height = images.size(image)
    new_image = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]

            red = pixel[0] * r
            if red > 255:
                red = 255

            green = pixel[1] * g
            if green > 255:
                green = 255

            blue = pixel[2] * b
            if blue > 255:
                blue = 255

            new_image[x][y] = (round(red), round(green), round(blue))
    return new_image


