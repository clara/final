from images import read_img, write_img
from transforms import rotate_right, mirror, blur, grayscale
import sys
'''Programa transform_simple.py, que aceptará dos parámetros: el nombre de un fichero de imagen en formato RGB, 
y el nombre de una función de transformación entre las siguients: rotate_right, mirror, blur, greyscale. 
El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero con el nombre y 
extensión del fichero de entrada, pero con el sufijo _trans en el nombre. 
Por ejemplo, si el fichero original se llama fichero.png, el fichero de salida se llamará fichero_trans.png. '''


def get_filename(filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename


def main():
    if len(sys.argv) < 3:
        print(f"\n[!] Uso: {sys.argv[0]} <imagen> <rotate_right, mirror, blur, grayscale>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        pixels = read_img(filename)
        new_pixels = pixels

        for transform in transforms:
            if not (transform == "rotate_right" or transform == "mirror" or transform == "blur" or transform == "grayscale"):
                print("\nUnknown transform!")
                print(f"\n[!]Uso: {sys.argv[0]} <imagen> <rotate_right, mirror, blur, grayscale>")
                sys.exit(1)

            else:
                if transform == "rotate_right":
                    new_pixels = rotate_right(new_pixels)
                elif transform == "mirror":
                    new_pixels = mirror(new_pixels)
                elif transform == "blur":
                    new_pixels = blur(new_pixels)
                else:
                    new_pixels = grayscale(new_pixels)

        new_filename = get_filename(filename)
        write_img(new_pixels, new_filename)

        print("[+] Transformacion exitosa!")


if __name__ == '__main__':
    main()
