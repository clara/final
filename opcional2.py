from images import size, create_blank


def sepia(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            original_r = pixel[0]
            original_g = pixel[1]
            original_b = pixel[2]
            sepia_r = round(0.393*original_r + 0.769*original_g + 0.189*original_b)
            sepia_g = round(0.349*original_r + 0.686*original_g + 0.168*original_b)
            sepia_b = round(0.272*original_r + 0.534*original_g + 0.131*original_b)
            pixel_sepia = (sepia_r, sepia_g, sepia_b)
            pixel_sepia_lista = list(pixel_sepia)
            for i in range(len(pixel_sepia)):
                if pixel_sepia_lista[i] < 0:
                    pixel_sepia_lista[i] = 0
                elif pixel_sepia_lista[i] > 255:
                    pixel_sepia_lista[i] = 255
            new_image[x][y] = pixel_sepia
    return new_image
