import sys
from transforms import *
from images import read_img, write_img
'''Programa trasnsform_args.py, que aceptará dos o más parámetros. El primer parámetro será el nombre de un fichero 
de imagen en formato RGB. El segundo parámetro será el nombre de una función de transformación cualquiera entre las 
que se hayan implementado. En caso de que la función admita parámetros, se incluirán a continuación. Para la función 
change_colors, serán seis números enteros, que serán los valores RGB a cambiar, y los valores RGB por los que 
se cambiarán. Para la función rotate_colors, será un número entero, el incremento que se aplicará a todos los colores. 
Para la función shift, serán dos números enteros, el número de pixels a desplazar horizontalmente y el número de pixels 
a desplazar verticalmente. El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un 
fichero con el nombre y extensión del fichero de entrada, pero con el sufijo _trans en el nombre. '''

transform_n_arguments = {"mirror": 0,
                         "blur": 0,
                         "rotate_right": 0,
                         "greyscale": 0,
                         "change_colors": 6,
                         "shift": 2,
                         "rotate_colors": 1,
                         "filter": 3,
                         "crop": 4}


def get_filename(filename: str) -> str:
    name = ".".join(filename.split(".")[:-1]) + "_trans"
    ext = filename.split(".")[-1]
    new_filename = name + "." + ext

    return new_filename


def apply_transform(pixels, transforms):
    transform = transforms[0]
    args = [int(i) for i in transforms[1:]]

    if transform_n_arguments[transform] == len(args):
        if transform == "rotate_right":
            new_pixels = rotate_right(pixels)
        elif transform == "mirror":
            new_pixels = mirror(pixels)
        elif transform == "blur":
            new_pixels = blur(pixels)
        elif transform == "grayscale":
            new_pixels = grayscale(pixels)
        elif transform == "change_colors":
            new_pixels = change_colors(pixels, (args[0], args[1], args[2]), (args[3], args[4], args[5]))
        elif transform == "shift":
            new_pixels = shift(pixels, args[0], args[1])
        elif transform == "rotate_colors":
            new_pixels = rotate_colors(pixels, args[0])
        elif transform == "filter":
            new_pixels = filter(pixels, args[0], args[1], args[2])
        elif transform == "crop":
            new_pixels = crop(pixels, args[0], args[1], args[2], args[3])

        return new_pixels

    else:
        print("[!] Número incorrecto de argumentos")
        sys.exit(1)


def main():
    if len(sys.argv) < 3:
        print(f"\n[!] Uso: {sys.argv[0]} <imagen> <transforms>")
        sys.exit(1)
    else:
        filename = sys.argv[1]
        transforms = sys.argv[2:]

        transform = transforms[0]

        if transform not in transform_n_arguments:
            print("Transformacion incorrecta")
            sys.exit(1)

        pixels = read_img(filename)
        new_pixels = apply_transform(pixels, transforms)

        new_filename = get_filename(filename)
        write_img(new_pixels, new_filename)

        print("[+] Transformacion exitosa!")


if __name__ == '__main__':
    main()